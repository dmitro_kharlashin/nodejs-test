let MessengerJs = require('./messenger.js')
    ,messenger = new MessengerJs()
    ,ChannelsJs = require('./channels.js')
    ,channels = new ChannelsJs()
    ,log4js = require('log4js')
    ,logger = log4js.getLogger()
    ,redis = require('redis')
    ,myEmitter = require('./appEvents.js');

function App() {
    function runAppWithoutArgs() {
        logger.level = 'debug';

        let sub = redis.createClient('6379', '127.0.0.1', {'prefix': 'nodejs-test:'})
            ,pub = redis.createClient('6379', '127.0.0.1', {'prefix': 'nodejs-test:'})
            ,data = new Date().toString()
            ,crypto = require('crypto')
            ,createdChannel = crypto.createHash('md5').update(data).digest("hex");

        sub.on('connect', function () {
            channels.addChannel(createdChannel);
        });

        sub.on("subscribe", function (channel) {
            if (channel !== 'channelsEventsMessages') {
                setInterval(() => {
                    (() => {
                        return new Promise(function (resolve, reject) {
                            channels.isChannelGenerator(resolve, reject);
                        })
                    })()
                        .then((isGenerator) => {
                            if (isGenerator === 'true') {
                                let selectedChannel = channels.getRandomChannel() || createdChannel;

                                return selectedChannel;
                            }
                            return null;
                        })
                        .catch((err) => {
                            logger.error(err);
                        })
                        .then((selectedChannel) => {
                            if(selectedChannel) {
                                let message = messenger.getMessage();
                                logger.debug("I`m generator "+createdChannel+", generate: "+ message);
                                pub.publish(selectedChannel, message);
                            }
                        })
                        .catch((err) => {
                            logger.error(err);
                        })
                }, 500);
            }
        });

        sub.on("message", function (channel, message) {
            if (channel === 'channelsEventsMessages') {
                let messageInfo = message.split("_")
                    ,newGenerator = messageInfo[0]
                    ,newCnt = messageInfo[1];
                logger.warn("Generator was fall down!");
                if (createdChannel === newGenerator) {
                    messenger.setCnt(newCnt);
                    logger.warn("Now, I`m generator "+newGenerator+", starting from "+newCnt);
                }
            } else {
                eventHandler(message, messenger.saveAndDisplayMessage);
            }
        });

        sub.subscribe(createdChannel);
        sub.subscribe('channelsEventsMessages');

        function eventHandler(msg, callback){
            function onComplete(){
                let error = Math.random() > 0.85;
                callback(error, msg);
            }
            // processing takes time...
            setTimeout(onComplete, Math.floor(Math.random()*1000));
        }

        function exitProcess() {
            let mainPromise = Promise.resolve();
            let beforeExitPromise = mainPromise.then(_=>{
                return new Promise((resolve, reject)=>{
                    myEmitter.emit('beforeExitProcess', pub, sub, createdChannel, messenger.cnt, resolve);
                })
            });
            beforeExitPromise.then(_=>{
                logger.warn("Bye bye!");
                process.exit(0);
            });
        }
        process.on('SIGINT', () => {
            exitProcess();
        }).on('SIGHUP', () => {
            exitProcess();
        }).on('SIGBREAK', () => {
            exitProcess();
        });
    }
    function runAppWithArgs(arg) {
        let scriptFunction;

        switch (arg){
            case 'getErrors':
                scriptFunction = function (callback) {
                    messenger.displayAndDeleteMessages('errors', callback);
                }
                break;
            case 'getMessages':
                scriptFunction = function (callback) {
                    messenger.displayAndDeleteMessages('messages', callback);
                }
                break;
            case 'getChannels':
                scriptFunction = function (callback) {
                    channels.displayChannelsList(callback);
                }
                break;
            case 'resetChannels':
                scriptFunction = function (callback) {
                    channels.deleteChannelsList(callback);
                }
                break;
            default:
                scriptFunction = function (callback) {
                    logger.error("Not accepted input parameter!");
                    if(callback)
                        callback();
                };
        }

        scriptFunction(_=>{process.exit(0)})
    }
    this.init = function (arg) {
        if (arg) {
            runAppWithArgs(arg);
        } else {
            runAppWithoutArgs();
        }
    }
};

module.exports = new App();