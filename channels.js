let redis = require('redis')
    ,client = redis.createClient('6379', '127.0.0.1', {'prefix': 'channels-list:'})
    ,log4js = require('log4js')
    ,logger = log4js.getLogger()
    ,_ = require('underscore');

logger.level = 'info';

function Channels()
{
    this.channelName = '';
    this.isChannelGenerator = function(resolve, reject){
        let q = ['channel:'+ this.channelName, 'isGenerator', 'lastTime'];

        client.hmget(q, function(err, repl) {
            if (err) return reject(err);
            let isGenerator = repl[0];

            return resolve(isGenerator);
        });
    };
    this.reattachNewGenerator = function(resolve, reject){
        let self = this;

        (()=>{return new Promise(function(resolve, reject)
        {
            self.getChannelsList(resolve, reject);
        })})()
            .then(function(channelsList){
                let newGenerator = _.first(channelsList);

                if (newGenerator !== 'undefined') {
                    client.hmset(
                        'channel:'+ newGenerator, 'isGenerator', true, 'lastTime', new Date().getTime()
                    )
                }
                resolve(newGenerator);
            })
            .catch(function (err) {
                reject(err);
            })
    };
    this.addChannel = function(channel){
        let self = this;

        (()=>{return new Promise(function(resolve, reject)
        {
            self.getChannelsList(resolve, reject);
        })})()
            .then(function(object){
                let channelsList = object
                    ,isGenerator = (_.isEmpty(channelsList));

                client.multi([
                    ['hmset', 'channel:'+ channel, 'isGenerator', isGenerator, 'lastTime', new Date().getTime()],
                    ['sadd', 'channels:', channel],
                ]).exec(function (err, replies) {
                    if (err) return logger.error("Channel "+ channel +" was not created: "+err);

                    self.channelName = channel;
                    logger.debug("Channel "+ channel +" was created: "+ _.first(replies));
                });
            })
            .catch(function (err) {
                logger.error(err);
            })
    };
    this.getRandomChannel = function(){
        let self = this;

        let selectedChannel = (()=>{return new Promise(function(resolve, reject)
        {
            self.getChannelsList(resolve, reject);
        })})()
            .then(function(channelsList){
                return _.isEmpty(channelsList) ? null : channelsList[Math.floor(Math.random()*channelsList.length)];
            })
            .catch(function (err) {
                logger.error(err);
            })

        return selectedChannel;
    };
    this.getChannelsList = function (resolve, reject) {

        client.smembers('channels:', function(err, object) {
            if(err) return reject(err);

            return resolve(object);
        });
    };
    this.displayChannelsList = function (callback) {
        let self = this;

        (()=>{return new Promise(function(resolve, reject)
        {
            self.getChannelsList(resolve, reject);
        })})()
            .then(function(object){
                logger.info("Channels: "+ object);

                if(callback)
                    callback();
            })
            .catch(function (err) {
                logger.error(err);
            })
    };
    this.deleteChannel = function (resolve, reject) {
        client.multi([
            ['del', 'channel:'+ this.channelName],
            ['srem', 'channels:', this.channelName],
        ]).exec(function (err, replies) {
            if (err) return reject(err);

            return resolve(replies);
        });
    };
    this.deleteChannelsList = function (callback) {
        let self = this;
        return (()=>{return new Promise(function(resolve, reject)
        {
            self.getChannelsList(resolve, reject);
        })})()
            .then(function(object){
                _.each(object, (item)=>{
                    logger.info("Channel: "+item+" will be deleted!");
                    client.del('channel:'+ item, function (err) {
                        if(err) logger.error(err);
                    });
                });
                client.del('channels:', function(err, reply) {
                    if(err) logger.error(err);

                    logger.debug("Channels list deleted: "+reply);
                });
                if(callback)
                    return callback();
            })
            .catch(function (err) {
                logger.error(err);
            })
    };
}

module.exports = Channels;