let redis = require('redis')
    ,client = redis.createClient('6379', '127.0.0.1', {'prefix': 'nodejs-test:'})
    ,log4js = require('log4js')
    ,logger = log4js.getLogger()
    ,_ = require('underscore');

logger.level = 'info';

function Messenger()
{
    this.cnt = 0;
    this.setCnt = function (value) {
        this.cnt = value;
    };
    this.getMessage = function(){
        this.cnt = this.cnt || 0;
        return this.cnt++;
    };
    function displayMessage(error, msg){
        logger.debug("I`m worker, receive: "+ msg);
    }
    function saveMessage(error, msg){
        let data = [];
        data.push((error ? 'errors' : 'messages'), msg);

        client.rpush(data);
    }
    this.saveAndDisplayMessage = function(error, msg){
        saveMessage(error, msg);
        displayMessage(error, msg);
    };
    this.getMessagesByType = function (type, resolve, reject) {
        client.lrange(type, 0, -1, function(err, object) {
            if(err) return reject(err);

            return resolve(object);
        });
    };
    this.deleteMessagesByType = function (type, resolve, reject) {
        client.del(type, function(err, reply) {
            if(err) return reject(err);

            return resolve(reply);
        });
    };
    this.displayAndDeleteMessages = function(type, callback){
        let self = this;


        return (()=>{return new Promise(function(resolve, reject)
        {
            self.getMessagesByType(type, resolve, reject);
        })})()
        .then(function(res)
        {
            let list = res.join(',');
            logger.info("List of "+type+": "+(list ? list : "is empty"));
        })
        .then(()=>{
            return new Promise(function(resolve, reject)
            {
                self.deleteMessagesByType(type, resolve, reject);
            })
        })
        .then(function (res) {
            if(callback)
                callback();
            return res;
        })
        .catch(function(err){
            logger.error(err);
        });
    };
    this.getLastSaveMessage = function () {
        let lastItem = 0;
        client.lrange('messages', 0, -1, function(err, object) {
            if(err) return logger.error(err);

            lastItem = _.last(object);
        });

        return lastItem;
    };
}

module.exports = Messenger;