const EventEmitter = require('events')
    ,myEmitter = new EventEmitter()
    ,log4js = require('log4js')
    ,logger = log4js.getLogger()
    ,ChannelsJs = require('./channels.js')
    ,channels = new ChannelsJs()
    ,MessengerJs = require('./messenger.js')
    ,messenger = new MessengerJs();

function beforeExit(pub, sub, channel, cnt, callback) {
    logger.warn("Exit process started!");

    channels.channelName = channel;
    let mainPromise = Promise.resolve();
    let checkingGeneratorStatusPromise = mainPromise.then(_=>{
        return new Promise((resolve, reject)=>{
            logger.warn("Exit step 1: checking is current channel is generator");
            channels.isChannelGenerator(resolve, reject);
        })
    });
    mainPromise.then(_=>{
        return new Promise((resolve, reject)=>{
            logger.warn("Exit step 2: delete current channel data from redis");
            sub.unsubscribe(channel);
            sub.unsubscribe('channelsEventsMessages');
            channels.deleteChannel(resolve, reject);
        });
    });
    let attachingNewGeneratorPromise = checkingGeneratorStatusPromise.then(isGenerator=>{
        return new Promise((resolve, reject)=>{
            logger.warn("Exit step 3: reattach new generator, if it needs: "+isGenerator);
            if (isGenerator === 'true') {
                channels.reattachNewGenerator(resolve, reject);
            } else {
                resolve(null);
            }
        })
    });
    attachingNewGeneratorPromise.then(newGenerator=>{
        logger.warn("Exit step 4: receive info with new generator data: "+ newGenerator);
        if (newGenerator !== 'undefined' && newGenerator.length > 0) {
            let message = newGenerator + "_" + cnt;
            pub.publish('channelsEventsMessages', message);
        }
        logger.warn("Exit step 5: finish all before exit actions");
        sub.quit();
        pub.quit();
    });
    attachingNewGeneratorPromise.then(_=>{
        logger.warn("Exit process near finish!");
        callback();
    }).catch(error=>logger.error(error));
}

myEmitter.on('beforeExitProcess', (pub, sub, channel, cnt, callback) => {
    beforeExit(pub, sub, channel, cnt, callback);
});

module.exports = myEmitter;